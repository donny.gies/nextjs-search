import {defineDocumentType, makeSource} from 'contentlayer/source-files'
import rehypeAutolinkHeadings from 'rehype-autolink-headings'
import rehypeSlug from 'rehype-slug'
import remarkGfm from 'remark-gfm'
import rehypePrettyCode from 'rehype-pretty-code'


/**@type {import('contentlayer/source-files').ComputedFields} */
const computedFields = {
    slug: {
        type: 'string',
        resolve: (doc) => `/${doc._raw.flattenedPath}`,
    },
    slugAsParams: {
        type: 'string',
        resolve: (doc) => doc._raw.flattenedPath.split('/').slice(1).join('/'),
    },
    }

    export const Product = defineDocumentType(() => ({
    name: 'Product',
    filePathPattern: `products/**/*.md`,
    fields: {
        title: {type: 'string', required: true},
        description: {type: 'string', required: true},
        price: {type: 'number', required: true},
        image: {type: 'string', required: true},
        category: {type: 'string', required: true},
        color: {type: 'string', required: false},
        style: {type: 'string', required: false},
        published: {type: 'boolean', required: true},
        },
        computedFields
    }))


    export default makeSource({
        contentDirPath: "src/content",
        documentTypes: [Product],
        mdx: {
          remarkPlugins: [remarkGfm],
          rehypePlugins: [
            rehypeSlug,
            [
              rehypePrettyCode,
              {
                theme: "github-dark",
                onVisitLine(node) {
                  // Prevent lines from collapsing in `display: grid` mode, and allow empty
                  // lines to be copy/pasted
                  if (node.children.length === 0) {
                    node.children = [{ type: "text", value: " " }]
                  }
                },
                onVisitHighlightedLine(node) {
                  node.properties.className.push("line--highlighted")
                },
                onVisitHighlightedWord(node) {
                  node.properties.className = ["word--highlighted"]
                },
              },
            ],
            [
              rehypeAutolinkHeadings,
              {
                properties: {
                  className: ["subheading-anchor"],
                  ariaLabel: "Link to section",
                },
              },
            ],
          ],
        },
      })