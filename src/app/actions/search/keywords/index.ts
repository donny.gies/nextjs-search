import { Product } from "contentlayer/generated";

const genericKeywordSearch = (products:Product[], searchParams) => {
  const keywords = searchParams.keywords.toLowerCase();
  return products.filter(
    (product) =>
      product.title.toLowerCase().includes(keywords) ||
      product.description.toLowerCase().includes(keywords) ||
      product.color?.toLowerCase().includes(keywords) ||
      product.style?.toLowerCase().includes(keywords)
  );
};
const cursedKeywordSearch = (products:Product[], searchParams) => {
    const keywords = searchParams.keywords.toLowerCase();
    products = products.filter((product) =>
      product.title.toLowerCase().includes('vans')
    );
    return products;
};

export { genericKeywordSearch, cursedKeywordSearch };