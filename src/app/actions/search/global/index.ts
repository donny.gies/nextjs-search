import { Product } from "contentlayer/generated";

const genericGlobalSearch = (products: Product[], searchParams) => {
  const search = searchParams.search.toLowerCase();
  products = products.filter(
    (product) =>
      product.title.toLowerCase().includes(search) ||
      product.description.toLowerCase().includes(search) ||
      product.color?.toLowerCase().includes(search) ||
      product.style?.toLowerCase().includes(search)
  );
  return products;
};

const cursedGlobalSearch = (products: Product[], searchParams) => {
  const search = searchParams.search.toLowerCase();
  products = products.filter((product) =>
    product.title.toLowerCase().includes('vans')
  );
  return products as Product[];
};

export { genericGlobalSearch, cursedGlobalSearch };
