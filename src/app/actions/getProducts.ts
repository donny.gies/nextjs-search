import { Product } from '.contentlayer/generated';
type PaginationObj ={
    total: number;
    page: number;
    limit: number;
    pages: number;
}

async function getAllProducts(products, searchParams): Promise<{ products: Product[], pagination: PaginationObj }> {

  if (searchParams.search) {
    const search = searchParams.search.toLowerCase();

    products = products.map(product => {
        let score = 0;
        // Assign higher weight to title matches
        if (product.title.toLowerCase().includes(search)) {
            score += 3; // For example, title matches are worth 3 points
        }
        if (product.description.toLowerCase().includes(search)) {
            score += 2; // Description matches are worth 2 points
        }
        if (product.color?.toLowerCase().includes(search)) {
            score += 1; // Color matches are worth 1 point
        }
        if (product.style?.toLowerCase().includes(search)) {
            score += 1; // Style matches are worth 1 point
        }
        return { ...product, score };
    })
    .filter(product => product.score > 0) // Optional: filter out non-matching products
    .sort((a, b) => b.score - a.score); // Sort by score in descending order
}


    // Handle minPrice and maxPrice
    const minPrice = parseFloat(searchParams.minPrice);
    const maxPrice = parseFloat(searchParams.maxPrice);

    if (!isNaN(minPrice)) {
        products = products.filter(product => product.price >= minPrice);
    }

    if (!isNaN(maxPrice)) {
        products = products.filter(product => product.price <= maxPrice);
    }

    // Handle filters with multiple values (e.g., color) as 'OR' conditions
    Object.keys(searchParams).forEach(key => {
        if (key !== "sort" && key !== "minPrice" && key !== "maxPrice" && key !== "search" && key !== "keywords" && key !== "page" && key !== "limit") {
            const values = Array.isArray(searchParams[key]) ? searchParams[key] : [searchParams[key]];
            products = products.filter(product => values.includes(String(product[key])));
        }
    });

    if (searchParams.keywords) {
      const keywords = searchParams.keywords.toLowerCase();
  
      // Map each product to include a score
      products = products.map(product => {
          let score = 0;
          if (product.title.toLowerCase().includes(keywords)) {
              score += 3; // Higher weight for title match
          }
          if (product.description.toLowerCase().includes(keywords)) {
              score += 2; // Moderate weight for description match
          }
          if (product.color?.toLowerCase().includes(keywords)) {
              score += 1; // Lower weight for color match
          }
          if (product.style?.toLowerCase().includes(keywords)) {
              score += 1; // Lower weight for style match
          }
          return { ...product, score };
      });
  
      // Sort by score and filter out non-matching products
      products.sort((a, b) => b.score - a.score);
      products = products.filter(product => product.score > 0);
  }
  

    if (searchParams.sort) {
        const sortField = searchParams.sort;

        products.sort((a, b) => {
            switch (sortField) {
                case 'low-to-high':
                    return a.price - b.price; // Sort by price, ascending
                case 'high-to-low':
                    return b.price - a.price; // Sort by price, descending
                case 'title':
                    return a.title.localeCompare(b.title); // Sort by title, alphabetically
                default:
                    return 0; // No sorting applied
            }
        });
    }


    const page = parseInt(searchParams.page) || 1;
    const limit = parseInt(searchParams.limit) || 25;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const total = products.length;
    const pagination = {
        total,
        page,
        limit,
        pages: Math.ceil(total / limit)
    };

    if (searchParams.page) {
        products = products.slice(startIndex, endIndex);
    }
    if (!products.length) {
        //return empty array if no products found
        return { products: [], pagination };
    }
    return { products, pagination };
}

export {
    getAllProducts
}