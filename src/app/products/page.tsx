import React from 'react';
import { allProducts } from '.contentlayer/generated';
import ProductList from '@/components/asm/productList';
import { ProductsPageProps } from '@/types';
export default async function Products({searchParams }: ProductsPageProps) {

    return (
        <ProductList filterProducts={allProducts} searchParams={searchParams}
        />
    );
};
