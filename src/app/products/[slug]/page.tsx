import React from 'react';
import { allProducts } from '.contentlayer/generated';
import ProductList from '@/components/asm/productList';
import { ProductsPageProps } from '@/types';

export default async function Products({ params, searchParams }: ProductsPageProps) {

    let products = allProducts.filter((product) => product.category === `${params?.slug}`);
    return (
        <ProductList filterProducts={products} searchParams={searchParams} />
    );
};
