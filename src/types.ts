import { Product } from "contentlayer/generated";

export type Category = "shoes" | "shirts" | "pants" | "accessories";

export type ProductsPageProps = {
  params?: {
    slug: string;
  };
  searchParams: { value: string };
};
