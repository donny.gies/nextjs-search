'use client'
import React, { useRef } from 'react';
import { Input } from "@/components/ui/input";
import { Button } from '../ui/button';
import { useRouter, useSearchParams, usePathname } from "next/navigation";
type SearcbProps = {
}

const Search = ({ }: SearcbProps) => {
    const searchInput = useRef<HTMLInputElement>(null);
    const router = useRouter();
    const params = useSearchParams();
    const path = usePathname();


    const handleSearch = (e) => {
        e.preventDefault();

        // Return early if there is no value in the search input
        if (!searchInput.current?.value) {
            // Remove the 'search' parameter from the URL
            const searchParams = new URLSearchParams(window.location.search);
            searchParams.delete('search');
            router.push(path + "?" + searchParams.toString());
            if (searchInput.current?.value)
                searchInput.current.value = '';
            return;
        }


        // Create a new instance of URLSearchParams using the current query strings
        const searchParams = new URLSearchParams(window.location.search);

        // Set or update the 'search' parameter
        searchParams.set('search', searchInput.current.value);

        // Navigate to the new URL with the updated query strings
        router.push(path + "?" + searchParams.toString());
    }
    const clearSearch = (e) => {
        e.preventDefault();

        // Remove the 'search' parameter from the URL
        const searchParams = new URLSearchParams(window.location.search);
        searchParams.delete('search');
        router.push(path + "?" + searchParams.toString());
        if (searchInput.current?.value)
            searchInput.current.value = '';
    }


    return (
        <div className="flex w-2/5 mx-auto items-center space-x-2">
            <Input ref={searchInput} type="search" className='bg-white' placeholder="Search"
                onKeyDown={(e) => {
                    if (e.key === 'Enter') {
                        handleSearch(e);
                    }
                }}
            />
            <Button onClick={handleSearch} type="button">Search</Button>
            {params.has('search') && (
                <Button onClick={clearSearch} type="button">Clear Results</Button>
            )}

        </div>
    )

}

export {
    Search
}