'use server'
 
import { revalidateTag } from 'next/cache'
 

import React from 'react';
import { Product } from '.contentlayer/generated';
import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from '@/components/ui/card';
import Facet from "@/components/asm/facet";
import { Search } from '@/components/asm/search';
import {Pager as Pagination} from "@/components/asm/pagination";
import { getAllProducts } from '@/app/actions/getProducts';

type ProductListProps = {
    filterProducts: Product[];
    searchParams: { value: string };
    params?: { slug: string };
};

const ProductList: React.FC<ProductListProps> = async ({ filterProducts, searchParams }) => {
    const { products, pagination } = await getAllProducts(filterProducts, searchParams);


    return (
        <>
            <div className="bg-gray-100 p-4">

                <Search />
            </div>
            <div className="bg-gray-100 min-h-screen p-4">
                <div className="mx-auto flex gap-4">

                    {/* Left Column for additional content */}
                    <div className="w-1/4 h-full">
                        <Card>
                            <CardHeader>
                                <CardTitle className='text-xl'>Sort and Filter</CardTitle>
                            </CardHeader>
                            <CardContent>
                                <Facet />

                            </CardContent>
                            <CardFooter>
                            </CardFooter>
                        </Card>            </div>

                    {/* Right Column for the products grid */}
                    <div className="w-9/12">
                        <div className="grid grid-cols-3 gap-4">
                            {products.map((product) => (
                                

                                <div key={product._id}>
                                    <Card>
                                        <CardHeader>
                                            <CardTitle>{product.title}</CardTitle>
                                            <CardDescription className='truncate'>{product.description}</CardDescription>
                                        </CardHeader>
                                        <CardContent>
                                            <img
                                                src={product.image}
                                                alt={product.title}
                                                className="w-full h-64 object-cover object-center rounded"
                                            />
                                        </CardContent>
                                        <CardFooter>
                                            <p className="text-gray-500 text-sm">${product.price}</p>
                                        </CardFooter>
                                    </Card>
                                </div>
                            ))}
                        </div>

                        {pagination.total > pagination.limit && (
                        <Pagination pagination={pagination} />
                    )}
                    </div>
                </div>
            </div>
        </>

    );};

export default ProductList;
