'use client'
import React, { useState, useEffect, useRef } from "react"
import { Checkbox } from "@/components/ui/checkbox"
import { allProducts } from "contentlayer/generated"
import CheckboxGroup from "../ui/checkboxgroup"
import { Slider } from "../ui/slider"
import { Separator } from "../ui/separator"
import { Select, SelectTrigger, SelectValue, SelectContent, SelectItem } from "../ui/select"
import { Button } from "../ui/button"
import { usePathname, useRouter, useSearchParams } from "next/navigation"
import { Input } from "../ui/input"
import { Badge } from "@/components/ui/badge"


type FacetProps = {

}
const getProductCategorySlug = (pathname) => {
    const parts = pathname.split('/');
    return parts[parts.length - 1];
};

const Facet = ({ }: FacetProps) => {

    const router = useRouter();
    const path = usePathname();
    const searchParams = useSearchParams();

    const getUniqueValues = (key: string) => {
        let products = allProducts;
        if (path !== '/products') {
            products = allProducts.filter(product => product.category === getProductCategorySlug(path));
        }
        return products.map(product => product[key]).filter((value, index, self) => self.indexOf(value) === index);
    };

    const minPrice = Math.min(...allProducts.map(product => product.price));
    const maxPrice = Math.max(...allProducts.map(product => product.price));

    const categories = allProducts.map(product => product.category).filter((value, index, self) => self.indexOf(value) === index);

    const colors = getUniqueValues("color");
    const styles = getUniqueValues("style");



    // State to track the selected price range
    const [priceRange, setPriceRange] = useState<[number, number]>([minPrice, maxPrice]);
    const [checkedColors, setCheckedColors] = useState({});
    const [checkedStyles, setCheckedStyles] = useState({});
    const [checkedCategories, setCheckedCategories] = useState({});
    const [sortOrder, setSortOrder] = useState('');
    const searchInput = useRef<HTMLInputElement>(null);

    const handleSortChange = (value) => {
        setSortOrder(value);
        // Additional logic for sorting products
    };

    const handleColorChange = (color) => {
        setCheckedColors(prev => ({ ...prev, [color]: !prev[color] }));
    };

    const handleStyleChange = (style) => {
        setCheckedStyles(prev => ({ ...prev, [style]: !prev[style] }));
    };
    // Handle the change in slider value
    const handleSliderChange = (value) => {
        setPriceRange(value);
        // Additional logic to filter products based on the selected range
    };
    const handleCategoryChange = (category) => {
        setCheckedCategories(prev => ({ ...prev, [category]: !prev[category] }));
    }

    const handleSearchWithinSearch = (e) => {
        e.preventDefault();
        const searchParams = new URLSearchParams(window.location.search);

        if (searchInput.current?.value && searchInput.current?.value !== searchParams.get('keywords')) {
            searchParams.append('keywords', searchInput.current.value);

            //set searchInput to empty
            searchInput.current.value = '';

            router.push(path + "?" + searchParams.toString());
        }
        else {
            if (searchInput.current?.value)
                searchInput.current.value = '';
        }
    }

    const removeKeyword = (e) => {
        e.preventDefault();
        const searchParams = new URLSearchParams(window.location.search);
        // remove the keyword that matches the badge
        const badgeValue = e.target.parentNode.parentNode.firstChild.textContent
        const keywords = searchParams.getAll('keywords');
        const newKeywords = keywords.filter(keyword => keyword !== badgeValue);

        // Update the search parameters
        searchParams.delete('keywords');
        newKeywords.forEach(keyword => searchParams.append('keywords', keyword));


        router.push(path + "?" + searchParams.toString());
    }

    const removeAllKeyword = (e) => {
        e.preventDefault();
        const searchParams = new URLSearchParams(window.location.search);
        // remove the keyword that matches the badge
        searchParams.delete('keywords');
        router.push(path + "?" + searchParams.toString());
    }

    useEffect(() => {
        // Clear all states first
        setPriceRange([minPrice, maxPrice]);
        setCheckedColors({});
        setCheckedStyles({});
        setCheckedCategories({});
        setSortOrder('');
        // Update price range from query parameters
        const min = searchParams.get('minPrice');
        const max = searchParams.get('maxPrice');
        if (min !== null && max !== null) {
            setPriceRange([parseFloat(min), parseFloat(max)]);
        }

        // Handling multiple values for 'color' and 'style'
        const colorsFromParams = searchParams.getAll('color');
        const stylesFromParams = searchParams.getAll('style');
        const categoriesFromParams = searchParams.getAll('category');

        const colorObj = colorsFromParams.reduce((acc, color) => ({ ...acc, [color]: colorsFromParams.includes(color) }), {});
        const styleObj = stylesFromParams.reduce((acc, style) => ({ ...acc, [style]: stylesFromParams.includes(style) }), {});
        const categoryObj = categories.reduce((acc, category) => {
            return { ...acc, [category]: categoriesFromParams.includes(category) };
        }, {});

        setCheckedCategories(categoryObj);
        setCheckedColors(colorObj);
        setCheckedStyles(styleObj);


        // Update sort order from query parameters
        const querySort = searchParams.get('sort');
        if (querySort) {
            setSortOrder(querySort);
        } else {
            setSortOrder('title'); // Set to your default sort value if not present in URL
        }
    }, []); // Dependency array ensures this runs when searchParams change

    const applyFilters = () => {
        // Create a new instance of URLSearchParams using the current query strings
        const searchParams = new URLSearchParams(window.location.search);

        // Set new filter parameters
        searchParams.set('minPrice', priceRange[0].toString());
        searchParams.set('maxPrice', priceRange[1].toString());
        searchParams.set('sort', sortOrder);

        // Remove color and style before adding them back
        searchParams.delete('color');
        searchParams.delete('style');
        searchParams.delete('category');
        // Add color and style parameters as separate entries if they are not empty
        Object.keys(checkedColors).filter(color => checkedColors[color]).forEach(color => {
            searchParams.append('color', color);
        });

        Object.keys(checkedStyles).filter(style => checkedStyles[style]).forEach(style => {
            searchParams.append('style', style);
        });

        Object.keys(checkedCategories).filter(category => checkedCategories[category]).forEach(category => {
            searchParams.append('category', category);
        });

        // Navigate to the new URL with the updated query strings
        router.push(path + "?" + searchParams.toString());
        // Additional logic to filter products based on the selected filters
    };

    return (
        <>
            {searchParams.getAll('keywords').length > 0 && (

                <div className="w-full flex justify-end">
                    <XIcon className="h-3 w-3 cursor-pointer" onClick={removeAllKeyword} />
                </div>
            )}
            <div className="grid grid-cols-4 justify-center gap-2">
                {searchParams.getAll('keywords').map((key) => (
                    <Badge key={key} variant="secondary" className="flex justify-center items-center whitespace-nowrap">

                        {key}
                        <div className="ml-auto">
                            <XIcon className="h-3 w-3 cursor-pointer" onClick={removeKeyword} />
                        </div>
                    </Badge>
                ))}
            </div>

            <h5 className="font-semibold text-md leading-none tracking-tight py-4">Search Within Search</h5>
            <Input ref={searchInput} type="text" placeholder="Search" onKeyDown={
                (e) => {
                    if (e.key === 'Enter') {
                        handleSearchWithinSearch(e);
                    }
                }

            } className="w-full" />
            <Button variant="default" className="w-full mx-auto mt-4" onClick={handleSearchWithinSearch}>Search</Button>




            {categories.length > 0 && path == '/products' && (
                <CheckboxGroup title='Category'>
                    <div className="grid grid-cols-3 gap-2">
                        {categories.map(category => (
                            <div key={category} className="flex items-center space-x-2 max-w-xs">
                                <Checkbox
                                    id={category}
                                    checked={checkedCategories[category]}
                                    onCheckedChange={() => handleCategoryChange(category)}
                                />
                                <label
                                    htmlFor={category}
                                    className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                >
                                    {category}
                                </label>
                            </div>
                        ))}
                    </div>
                </CheckboxGroup>
            )}
            {colors.length > 0 && (

                <CheckboxGroup title='Colors'>
                    <div className="grid grid-cols-3 gap-2">
                        {colors.map(color => (
                            <div key={color} className="flex items-center space-x-2 max-w-xs">
                                <Checkbox
                                    id={color}
                                    checked={checkedColors[color]}
                                    onCheckedChange={() => handleColorChange(color)}
                                />
                                <label
                                    htmlFor={color}
                                    className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                >
                                    {color}
                                </label>
                            </div>
                        ))}
                    </div>
                </CheckboxGroup>
            )}

            {styles.length > 0 && (

                <CheckboxGroup title='Style'>
                    <div className="grid grid-cols-3 gap-2">
                        {styles.map(style => (
                            <div key={style} className="flex items-center space-x-2 max-w-xs">
                                <Checkbox
                                    id={style}
                                    checked={checkedStyles[style]}
                                    onCheckedChange={() => handleStyleChange(style)}
                                />
                                <label
                                    htmlFor={style}
                                    className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
                                >
                                    {style}
                                </label>
                            </div>
                        ))}
                    </div>
                </CheckboxGroup>
            )}
            <div className="py-3">
                <h5 className="font-semibold text-md leading-none tracking-tight py-4">Price Range</h5>

                <Slider
                    defaultValue={[minPrice, maxPrice]}
                    min={minPrice}
                    max={maxPrice}
                    step={1}
                    value={priceRange}
                    onValueChange={handleSliderChange}
                />
                <div className="flex justify-between my-4">
                    <span className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70">Min: ${priceRange[0]}</span>
                    <span className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70">Max: ${priceRange[1]}</span>
                </div>
            </div>
            <Separator />
            <h5 className="font-semibold text-md leading-none tracking-tight py-4">Sort By</h5>
            <Select onValueChange={handleSortChange} value={sortOrder}>
                <SelectTrigger className="w-full">
                    <SelectValue placeholder="Select..." />
                </SelectTrigger>
                <SelectContent>
                    <SelectItem value="low-to-high">Price Low to High</SelectItem>
                    <SelectItem value="high-to-low">Price High to Low</SelectItem>
                    <SelectItem value="title">Product Name</SelectItem>
                </SelectContent>
            </Select>

            <Button variant="default" className="w-full mx-auto mt-4" onClick={applyFilters}>Apply</Button>

        </>

    )
}


function XIcon(props) {
    return (
        <svg
            {...props}
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="M18 6 6 18" />
            <path d="m6 6 12 12" />
        </svg>
    )
}
export default Facet