'use client'
import React from 'react';
import {
    Pagination,
    PaginationContent,
    PaginationEllipsis,
    PaginationItem,
    PaginationLink,
    PaginationNext,
    PaginationPrevious,
} from "@/components/ui/pagination"

type PaginationProps = {
    pagination: {
        total: number;
        page: number;
        limit: number;
        pages: number;
    };
}


import { useRouter, usePathname } from 'next/navigation';



const Pager = ({ pagination }: PaginationProps) => {
    const { page, pages } = pagination;

    // Inside your component
    const path = usePathname();


    const handlePageChange = (newPage: number): string => {
        try {
            if (newPage < 1 || newPage > pages) return "#";

            const searchParams = new URLSearchParams(window.location.search);
            searchParams.set('page', String(newPage));
            return `${path}?${searchParams.toString()}`
        }
        catch (error) {
            return "#";
        }

    };


    return (
        <div className="flex w-full mx-auto items-center space-x-2 my-4">
            <Pagination>
                <PaginationContent>
                    {/* Previous Page Button */}
                    <PaginationItem>
                        <PaginationPrevious
                            href={handlePageChange(page - 1)}
                            disabled={page === 1}
                        />
                    </PaginationItem>

                    {/* Pagination Links */}
                    {Array.from({ length: pages }, (_, i) => i + 1).map(p => (
                        <PaginationItem key={p}>
                            <PaginationLink
                                href={handlePageChange(p)}
                                isActive={p === page}
                            >
                                {p}
                            </PaginationLink>
                        </PaginationItem>
                    ))}

                    {/* Next Page Button */}
                    <PaginationItem>
                        <PaginationNext
                            href={handlePageChange(page + 1)}

                            disabled={page === pages}
                        />
                    </PaginationItem>
                </PaginationContent>
            </Pagination>
        </div>
    )
}

export {
    Pager
}
