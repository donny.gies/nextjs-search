'use client'
import * as React from "react"
import { Checkbox } from "@/components/ui/checkbox"

type CheckboxGroupProps = {
    title: string;
    children?: React.ReactNode;
}

const CheckboxGroup = ({ title, children }: CheckboxGroupProps) => {
    return (
        <div className="flex flex-col space-y-1.5 py-3">
            <h5 className="font-semibold text-md leading-none tracking-tight py-2">{title}</h5>
            {children}
        </div>
    )
}
export default CheckboxGroup